#!/bin/bash
# cpufreq.bash
# Desenvolvido por: Keviny :-)
# Script desenvolvido para trocar de governor do cpufreq...
# sem interface grafica
# É necessário ser root para se exutar o script corretamente.
# Para usar:
# ./cpufreq.bash

principal() {
if [ `whoami` = "root" ];then
	while true; do
		clear
		echo "QUAL GOVERNOR DESEJA ESCOLHER ?"
		echo
        echo "1 - Exibir o Atual Perfil/Governor"
		echo "2 - Powersave"
		echo "3 - Conservative"
		echo "4 - Ondemand"
		echo "5 - Performance"
		echo "6 - Sair"
		echo
		echo -n "Opção: ";read op

		case $op in
            1)governor;;
			2)powersave;;
			3)conservative;;
			4)ondemand;;
			5)performance;;
			6)clear;exit;;
			*)echo;echo "Opção inválida";echo;;
		esac
		echo "Pressione uma tecla para continuar..."
		read a
	done
else
	echo "É necessário ser o root"
fi
}

governor() {
cont=1
	echo "Perfi Atual:"
	echo
	cat /sys/devices/system/cpu/cpu[0-1]/cpufreq/scaling_governor
}


powersave() {
cont=1
	echo "Perfil Anterior:"
	echo
	cat /sys/devices/system/cpu/cpu1/cpufreq/scaling_governor
	echo
	echo "Perfil Atual:"
	echo
	echo powersave > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
	echo powersave > /sys/devices/system/cpu/cpu1/cpufreq/scaling_governor
	cat /sys/devices/system/cpu/cpu[0-1]/cpufreq/scaling_governor
}

conservative() {
cont=1
	echo "Perfil Anterior:"
	echo
	cat /sys/devices/system/cpu/cpu1/cpufreq/scaling_governor
	echo
	echo "Perfil Atual:"
	echo
	echo conservative > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
	echo conservative > /sys/devices/system/cpu/cpu1/cpufreq/scaling_governor
	cat /sys/devices/system/cpu/cpu[0-1]/cpufreq/scaling_governor
	echo 
}

ondemand() {
cont=1
	echo "Perfil Anterior:"
	echo
	cat /sys/devices/system/cpu/cpu1/cpufreq/scaling_governor
	echo
	echo "Perfil Atual:"
	echo
	echo userspace > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
	echo userspace > /sys/devices/system/cpu/cpu1/cpufreq/scaling_governor
	cat /sys/devices/system/cpu/cpu[0-1]/cpufreq/scaling_governor
}

performance() {
cont=1
	echo "Perfil Anterior:"
	echo
	cat /sys/devices/system/cpu/cpu1/cpufreq/scaling_governor
	echo
	echo "Perfil Atual:"
	echo
	echo performance > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
	echo performance > /sys/devices/system/cpu/cpu1/cpufreq/scaling_governor
	cat /sys/devices/system/cpu/cpu[0-1]/cpufreq/scaling_governor
}

principal
