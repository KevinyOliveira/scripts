#!/bin/bash

echo "Digite o appid do jogo"
read pid

NOME_DESTINO="steam_icon_"$pid

cd /home/$USER/.local/share/icons/hicolor/

echo "gerando imagem 16x16..."
convert imagem.png -resize 16x16 $NOME_DESTINO".png" 

mv $NOME_DESTINO".png" 16x16/apps/

echo "gerando imagem 24x24..."
convert imagem.png -resize 24x24 $NOME_DESTINO".png" 

mv $NOME_DESTINO".png" 24x24/apps/

echo "gerando imagem 32x32..."
convert imagem.png -resize 32x32 $NOME_DESTINO".png" 

mv $NOME_DESTINO".png" 32x32/apps/

echo "gerando imagem 48x48..."
convert imagem.png -resize 48x48 $NOME_DESTINO".png" 

mv $NOME_DESTINO".png" 48x48/apps/

echo "gerando imagem 64x64..."
convert imagem.png -resize 64x64 $NOME_DESTINO".png" 

mv $NOME_DESTINO".png" 64x64/apps/

echo "gerando imagem 96x96..."
convert imagem.png -resize 96x96 $NOME_DESTINO".png" 

mv $NOME_DESTINO".png" 96x96/apps/

echo "gerando imagem 128x128..."
convert imagem.png -resize 128x128 $NOME_DESTINO".png" 

mv $NOME_DESTINO".png" 128x128/apps/

echo "gerando imagem 256x256..."
convert imagem.png -resize 256x256 $NOME_DESTINO".png" 

mv $NOME_DESTINO".png" 256x256/apps/
