#!/bin/bash
cp -v /usr/share/applications/exo-preferred-applications.desktop ~/.local/share/applications/
sudo rm -v /usr/share/applications/exo-*
cp -v /usr/share/applications/bssh.desktop ~/.local/share/applications/
echo "NoDisplay=true" >> ~/.local/share/applications/bssh.desktop
cp -v /usr/share/applications/bnvc.desktop ~/.local/share/applications/
echo "NoDisplay=true" >> ~/.local/share/applications/bnvc.desktop
cp -v /usr/share/applications/qv* ~/.local/share/applications/
echo "NoDisplay=true" >> ~/.local/share/applications/qvidcap.desktop
echo "NoDisplay=true" >> ~/.local/share/applications/qv4l2.desktop
cp -v /usr/share/applications/avahi-discover.desktop ~/.local/share/applications/
echo "NoDisplay=true" >> ~/.local/share/applications/avahi-discover.desktop
sudo update-desktop-database -v
