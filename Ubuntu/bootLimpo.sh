sudo update-rc.d -f bluetooth remove
sudo update-rc.d -f avahi-daemon remove
sudo update-rc.d -f cups remove
sudo update-rc.d -f cups-browsed remove
sudo update-rc.d -f apport remove
sudo update-rc.d -f apache2 remove
sudo update-rc.d -f mysql remove
sudo systemctl disable mysql
