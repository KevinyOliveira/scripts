# * ~/.bashrc Personalizado para Ubuntu *

# Se não estiver rodando interativamente, não fazer nada
[ -z "$PS1" ] && return

# Não armazenar as linhas duplicadas ou linhas que começam com espaço no historico
HISTCONTROL=ignoreboth

# Adicionar ao Historico e não substitui-lo
shopt -s histappend

# Definições do comprimento e tamnho do historico.
HISTSIZE=1000
HISTFILESIZE=2000

#===========================================
# Váriavies com as Cores
#===========================================
NONE="\[\033[0m\]" # Eliminar as Cores, deixar padrão)

## Cores de Fonte
R="\[\033[0;31m\]" # Red (Vermelho)
## Efeito Negrito (bold) e cores
BG="\[\033[1;32m\]" # Bold+Green (Negrito+Verde)

#=============================================
# Configurações referentes ao usuário
#=============================================

## Verifica se é usuário root (UUID=0) ou usuário comum
if [ $UID -eq "0" ]; then

## Cores e efeitos do Usuario root

PS1="\e[01;31;44m┌─\e[01;36m[\e[01;31m\u\e[01;36m]@\e[01;36m[\e[01;37m\h\e[01;36m]\e[01;36m[\e[01;33m\d\e[01;36m]@\e[01;36m[\e[01;35m\t\e[01;36m]\e[01;36m[\e[01;33m\w\e[01;36m]\e[01;31m\n└──>\e[01;31m\e[01;36m\e[00m \e[01;36;00m\e[00;m$R\\$ $NONE"

else

## Cores e efeitos do usuário comum


PS1="\e[01;32;41m┌─\e[01;36m[\e[01;32m\u\e[01;36m]@\e[01;36m[\e[01;37m\h\e[01;36m]\e[01;36m[\e[01;33m\d\e[01;36m]@\e[01;36m[\e[01;35m\t\e[01;36m]\e[01;36m[\e[01;33m\w\e[01;36m]\e[01;32m\n└──>\e[01;36m\e[01;36m\e[00m \e[01;36;00m\e[00m$BG\\$ $NONE"

fi # Fim da condição if

## Habilitando suporte a cores para o ls e outros aliases
## Vê se o arquivo existe
if [ -x /usr/bin/dircolors ]; then
test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"

## Aliases (apelidos) para comandos
alias ls='ls --color=auto'
alias dir='dir --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
fi # Fim do if do dircolor

## Aliases (apelidos) diversos

# atualizar o apt-get
alias upd='sudo apt-get update'
alias aptrepair='sudo apt-get -f install'
alias dpkgrepair='sudo dpkg --configure -a'
alias google='ping 8.8.8.8' # Ping ao google a cada 3 segundos
alias infosystem="inxi -FxZ"

neofetch
