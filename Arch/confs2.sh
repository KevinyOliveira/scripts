yay -S efibootmgr refind nvidia nvidia-settings xorg-server xorg-apps mesa dosfstools os-prober networkmanager wpa_supplicant samba smbclient smb4k plasma-desktop konsole kvantum kvantum-theme-akava-git sddm pipewire-pulse pipewire plasma-pa plasma-nm gnome-disk-utility firefox vlc playonlinux peek telegram-desktop sublime-text-4 spotify spotify-adblock flatpak spectacle lswh htop okular hardinfo nomacs kdeconnect kate gparted zsh zsh-syntax-highlighting zsh-completions zsh-autosuggestions packagekit-qt6 discover x11vnc bluez bluedevil noto-fonts-emoji inxi sddm-kcm kde-gtk-config breeze-gtk linux-headers dnsmasq adobe-source-han-sans-jp-fonts calc sshfs mtpfs ark zip unrar android-tools hddtemp libva-utils appmenu-gtk-module kdeplasma-addons vulkan-tools mangohud extra-cmake-modules gst-libav base-devel mpv python-websockets qt6-declarative qt6-websockets qt6-webchannel vulkan-headers cmake kscreen kinfocenter --noconfirm
flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
sudo systemctl enable sddm.service
sudo systemctl enable NetworkManager
sudo systemctl enable smb.service
sudo systemctl enable bluetooth.service
sudo gpasswd -a keviny-arch wheel

