loadkeys br-abnt2
#mkfs.fat -F 32 /dev/sda1
#mkfs.btrfs -f /dev/sda2
#mount /dev/sda2 /mnt
#mount --mkdir /dev/sda1 /mnt/boot
#mount --mkdir /dev/sdb1 /media
pacstrap -K /mnt base base-devel linux linux-firmware micro
genfstab -U /mnt >> /mnt/etc/fstab
cp -v /media/Scripts/Arch/fstab /mnt/etc/fstab
cp -v /media/Scripts/Arch/mirrorlist /mnt/etc/pacman.d/mirrorlist
cp -v /media/Scripts/Arch/pacman.conf /mnt/etc/pacman.conf
cp -v /media/Scripts/Arch//locale.gen /mnt/etc/locale.gen
cp -v /media/Scripts/Arch/locale.conf /mnt/etc/locale.conf
cp -v /media/Scripts/Arch/vconsole.conf /mnt/etc/vconsole.conf
cp -v /media/Scripts/Arch/hostname /etc/hostname
#cp -v /media/Scripts/Arch/passwd /etc/passwd
arch-chroot /mnt
ln -sf /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime
hwclock --systohc
locale-gen
useradd -m -g users -G storage,power -s /bin/zsh keviny-arch
passwd
